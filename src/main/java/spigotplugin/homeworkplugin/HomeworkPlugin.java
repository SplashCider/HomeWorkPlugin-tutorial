package spigotplugin.homeworkplugin;

import org.bukkit.Material;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Field;
import java.util.Random;

public class HomeworkPlugin extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {}

    @EventHandler
    public void onEntityDeathEvent(EntityDeathEvent event) {
        if ((!(event.getEntity() instanceof Monster)) && (!(event.getEntity() instanceof Ghast))) return;
        Double rate = OTHER_DROPRATE;
        try {
            Field field = this.getClass().getField(event.getEntityType().name() + "_DROPRATE");
            rate = field.getDouble(this);
        } catch (Exception e) {}
        if (Math.random() <= rate) event.getDrops().add(new ItemStack(Material.EMERALD, rand.nextInt(3) + 1));
    }

    public static Random rand = new Random();
    public static double ZOMBIE_DROPRATE = 0.2;
    public static double SKELETON_DROPRATE = 0.4;
    public static double CREEPER_DROPRATE = 0.9;
    public static double SPIDER_DROPRATE = 0.3;
    public static double ENDERMAN_DROPRATE = 0.7;
    public static double PIG_ZOMBIE_DROPRATE = 0.7;
    public static double GHAST_DROPRATE = 1.0;
    public static double SLIME_DROPRATE = 0.05;
    public static double MAGMA_CUBE_DROPRATE = 0.05;
    public static double WITCH_DROPRATE = 1.0;
    public static double OTHER_DROPRATE = 0.1;
}
